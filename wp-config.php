<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'bugados' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'bugo' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'bugo' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'https://bugados.netlify.com' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Xmw,@M9n,_qs&4!^y<<BHjX0XoIgRaW{NmSq0nCBiYBA(Z_+1iT!=NU&d&z*L34T' );
define( 'SECURE_AUTH_KEY',  'zplmI%N@SLRdv8PatoP4tUH(g:d~2qTau;&>M;Be>Z SFEi@Om`hc&P(>LldpKTS' );
define( 'LOGGED_IN_KEY',    'M%(r)kWe9>=V<icyfv6v.Cla?;mA8O`IBkaeRYY 0J!(w$JcBN2-6V]fz3065[KM' );
define( 'NONCE_KEY',        '6l7di=Cjc6@Osl^7>jY8A?yB+6[`Y<gv#Imr?y.Hf8RL6l@r~X#K~]{J=s,Twiz@' );
define( 'AUTH_SALT',        '~(e/HR{SVZa[!2nZhH[X]XRpQ_vp%bjU+ P#tyFE(bj;(cyQjT Gp==Hv%o9an@9' );
define( 'SECURE_AUTH_SALT', '=3Gw=l|~k@C@gwK{<)iJF+s}1,21~$/P^AL;G|Tr~G}gGFqTb?2za(i~a%,A`$dt' );
define( 'LOGGED_IN_SALT',   '>Uw(Urp-!S}N+D0{xooZ8zs(cri.(e;XGZ5Ns%PTUAYIY]NQSR?H_hcF-|4M35Nw' );
define( 'NONCE_SALT',       'A,ypyssj|~w(i0WtY2Os*R#G1U<!#FPr9G(dTy6AM6oP[ZkO)F}NRV/-><U{P@TQ' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'bg_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
